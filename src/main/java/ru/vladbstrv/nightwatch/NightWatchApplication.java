package ru.vladbstrv.nightwatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NightWatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(NightWatchApplication.class, args);
    }

}
